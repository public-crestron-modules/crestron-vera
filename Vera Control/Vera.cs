﻿using System;
using System.Text;
using Crestron.SimplSharp;      // For Basic SIMPL# Classes
using Crestron.SimplSharp.Net.Http;      // For Basic SIMPL# Classes
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Vera_Control
{
    public delegate void VeraPollEventHandler(VeraEventArgs e);

    public class Vera
    {
        static public string veraIP;
        static private String url;
        static private String switchUrl;
        static private string lockUrl;

        public static string version, serialNumber, tempUnits, skin;

        public static void GetVeraStatus()
        {
            url = string.Format("http://" + veraIP + ":3480/data_request?id=lu_sdata&loadtime=1282441735&dataversion=441736333&timeout=60&minimumdelay=2000");

            try
            {
                var httpSet = new HttpClient();
                httpSet.KeepAlive = false;
                httpSet.Accept = "text/html";
                HttpClientRequest sRequest = new HttpClientRequest();
                sRequest.Url.Parse(url);                                                        //send request URL
                HttpClientResponse sResponse = httpSet.Dispatch(sRequest);
                var jsontext = sResponse.ContentString;                                         //dump info from request response into variable

                JObject results = JObject.Parse(jsontext);

                foreach (var device in results["devices"])
                {
                    // this can be a string or null
                    string RxDeviceName = (string)device["name"];
                    ushort RxDeviceId = (ushort)device["id"];
                    int RxCategory = (int)device["category"];
                    string RxStatus = (string)device["status"];
                    string RxWatts = (string)device["watts"];
                    string RxKwh = (string)device["kwh"];
                    string RxComment = (string)device["comment"];
                    string RxLocked = (string)device["locked"];
                    string RxBattery = (string)device["batterylevel"];

                    VeraChangeEvents.VeraPollChange(RxDeviceName, RxStatus, RxWatts, RxKwh, RxDeviceId, RxComment, RxLocked, RxBattery);

                    // CrestronConsole.PrintLine("Device Name: {0}, Device Number: {1} in Category {2}", RxDeviceName, RxDeviceId, RxCategory);
                }
            }
            catch (Exception ex)
            {
                CrestronConsole.PrintLine(String.Format("Vera Exception Polling Device: {0}", ex.Message));
            }
        }

        public static void SwitchCommand(int id, int status)
        {
            switchUrl = string.Format("http://" + veraIP + ":3480/data_request?id=lu_action&output_format=json&DeviceNum=" + id + "&serviceId=urn:upnp-org:serviceId:SwitchPower1&action=SetTarget&newTargetValue=" + status);

            try
            {
                var httpSet = new HttpClient();
                httpSet.KeepAlive = false;
                httpSet.Accept = "application/x-www-form-urlencoded";
                HttpClientRequest sRequest = new HttpClientRequest();
                sRequest.Url.Parse(switchUrl);                                                        //send request URL
                HttpClientResponse sResponse = httpSet.Dispatch(sRequest);
            }

            catch (Exception ex)
            {
                CrestronConsole.PrintLine(String.Format("Vera Exception Setting Swtich: {0}"), ex.Message);
            }
        }

        public static void LockCommand(int id, int status)
        {
            lockUrl = string.Format("http://" + veraIP + ":3480/data_request?id=action&output_format=json&DeviceNum=" + id + "&serviceId=urn:micasaverde-com:serviceId:DoorLock1&action=SetTarget&newTargetValue=" + status);
            try
            {
                var httpSet = new HttpClient();
                httpSet.KeepAlive = false;
                httpSet.Accept = "application/x-www-form-urlencoded";
                HttpClientRequest sRequest = new HttpClientRequest();
                sRequest.Url.Parse(lockUrl);                                                        //send request URL
                HttpClientResponse sResponse = httpSet.Dispatch(sRequest);
            }

            catch (Exception ex)
            {
                CrestronConsole.PrintLine(String.Format("Vera Exception Setting Door Lock: {0}"), ex.Message);
            }
        }

        //Default constructor
        public Vera()
        {
        }
    }

    public class VeraEventArgs : EventArgs
    {
        public string vDeviceName { get; set; }

        public string vDeviceStatus { get; set; }

        public string vDeviceWatts { get; set; }

        public string vDeviceKwh { get; set; }

        public ushort vDeviceId { get; set; }

        public string vDeviceComment { get; set; }

        public string vDeviceLocked { get; set; }

        public string vDeviceBattery { get; set; }

        //Default Contructor
        public VeraEventArgs()
        {
        }

        public VeraEventArgs(string vDeviceName, string vDeviceStatus, string vDeviceWatts, string vDeviceKwh, ushort vDeviceId, string vDeviceComment, string vDeviceLocked, string vDeviceBattery)
        {
            this.vDeviceName = vDeviceName;
            this.vDeviceStatus = vDeviceStatus;
            this.vDeviceWatts = vDeviceWatts;
            this.vDeviceKwh = vDeviceKwh;
            this.vDeviceId = vDeviceId;
            this.vDeviceComment = vDeviceComment;
            this.vDeviceLocked = vDeviceLocked;
            this.vDeviceBattery = vDeviceBattery;
        }
    }

    public static class VeraChangeEvents
    {
        public static event VeraPollEventHandler onVeraPollChange;

        public static void VeraPollChange(string DeviceName, string DeviceStatus, string DeviceWatts, string DeviceKwh, ushort DeviceId, string DeviceComment, string DeviceLocked, string DeviceBattery)
        {
            VeraChangeEvents.onVeraPollChange(new VeraEventArgs(DeviceName, DeviceStatus, DeviceWatts, DeviceKwh, DeviceId, DeviceComment, DeviceLocked, DeviceBattery));
        }
    }
}